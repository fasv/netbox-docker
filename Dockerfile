FROM debian:bullseye
RUN apt-get update && apt-get install -y wget supervisor python3 python3-pip python3-venv python3-dev build-essential libxml2-dev libxslt1-dev libffi-dev libpq-dev libssl-dev zlib1g-dev
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
WORKDIR /opt
COPY netbox-3.3.0 ./netbox/
RUN adduser --system --group netbox
RUN chown --recursive netbox /opt/netbox/netbox/media/
RUN /opt/netbox/upgrade.sh
RUN . /opt/netbox/venv/bin/activate && DJANGO_SUPERUSER_USERNAME=admin DJANGO_SUPERUSER_EMAIL=admin@mail.ru DJANGO_SUPERUSER_PASSWORD=admin /opt/netbox/netbox/manage.py createsuperuser --noinput
EXPOSE 8001
CMD ["/usr/bin/supervisord"]